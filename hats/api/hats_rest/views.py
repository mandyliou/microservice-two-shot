from django.shortcuts import render
from common.json import ModelEncoder
from .models import LocationVO, Hat
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href"
        ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id"
    ]
    #helps add things that aren't necessarily part of the model
    def get_extra_data(self, o): #just adds extra property to JSONResponse
        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "id",
        "location"

    ]
    encoders = { #if properties  locations matches the key, will return the encoder
        "location": LocationVODetailEncoder(),
    }

@require_http_methods(["DELETE", "GET"]) #function only runs if it matches the method
#get detail or delete a hat
def api_show_hat(request, pk):
    if request.method == "GET":
            hat = Hat.objects.get(id=pk)
            return JsonResponse( #turns object into JSON
                hat,
                encoder=HatDetailEncoder, #converts to JSON
                safe=False
            )
    else:
        request.method == "DELETE"
        count, _ = Hat.objects.filter(id=pk).delete() #count how many you delete
        return JsonResponse({"deleted": count > 0}) #return true if greater than 0


#list of hats
@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET": #listing out the hats
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder = HatListEncoder,
        )
    else: #creating a hat
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href) #does it match
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
