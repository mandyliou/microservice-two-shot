# Wardrobify

Team:

* Caleb Lee - Shoes microservice
* Mandy Liou - Hats microservice

## Design
![Alt text](img/wardrobe.jpg)

## Instructions to run application
1. Fork the project from the respective gitlab repository
2. Open terminal and cd into desired folder to host the project
    - git clone <respository name>
3. Run the following commands after opening the project in the desired folder:
    - docker volume create pgdata (generates database)
    - docker-compose build (creates the images for the containers)
    - docker-compose up (creates containers and runs them with the respective images)

## Ports and URL paths
HATS MICROSERVICE:
| Method | URL | Expected Response Data | (PORT)

| GET | http://localhost:8090/api/hats/<int:pk>/ | Shows detail for a specific hat | (8080)
| GET | http://localhost:8090/api/hats/ | Shows a list of all the hats | (8080)
| POST | http://localhost:8090/api/hats/ | Creates a new hat |(8080)
| DELETE | http://localhost:8090/api/hats/<int:pk>/ | Delete a specific hat | (8080)

SHOES MICROSERVICE:
| Method | URL | Expected Response Data | (PORT)

| GET | http://localhost:8080/api/shoes/<int:pk>/ | Shows detail for a specific shoe | (8080)
| GET | http://localhost:8080/api/shoes/ | Shows a list of all the shoes | (8080)
| POST | http://localhost:8080/api/shoes/ | Creates a new shoe | (8080)
| DELETE | http://localhost:8080/api/shoes/<int:pk>/ | Delete a specific shoe | (8080)

WARDROBE MICROSERVICE (Bins & Locations):
| Method | URL | Expected Response Data | (PORT)

| GET | http://localhost:8100/api/bins/ | Gets a list of all of the bins | (8100)
| GET | http://localhost:8100/api/bins/<int:pk>/ | Gets the details of one bin | (8100)
| POST | http://localhost:8100/api/bins/ | Creates a new bin with the posted data | (8100)
| PUT | http://localhost:8100/api/bins/<int:pk>/ | Updates the details of one bin | (8100)
| DELETE | http://localhost:8100/api/bins/<int:pk>/ | Deletes a single bin | (8100)

| Method | URL | Expected Response Data | (PORT)

| GET | http://localhost:8100/api/locations/ | Gets a list of all of the locations | (8100)
| GET | http://localhost:8100/api/locations/<int:pk>/ | Gets the details of one location | (8100)
| POST | http://localhost:8100/api/locations/ | Creates a new location with the posted data | (8100)
| PUT | http://localhost:8100/api/locations/<int:pk>/ | Updates the details of one location | (8100)
| DELETE | http://localhost:8100/api/locations/<int:pk>/ | Deletes a single location | (8100)

SAMPLE POST DATA:
{
	"manufacturer": "Addidas",
	"model_name": "Superstars",
	"color": "blue",
	"picture_url": "https://images.pexels.com/photos/1070360/pexels-photo-1070360.jpeg?auto=compress&cs=tinysrgb&w=1600",
	"bin": "/api/bins/2/"

}

## Shoes microservice

MODELS:

BinVO: BinVO is a value object with respect to the Bin model which is located in the wardrobe microservice. The purpose of this model is to relate the Bin information to the specific shoe identity. The significance of having this value object lies in the fact that the shoe model directly pertains to the specific bin where the shoe exists. This relation is made possible through the construction of a ForeignKey in tandem with the poller, which serves to pull the bin data from the wardrobe api and into the shoes microservice.

Shoe: The shoe model serves as an entity in which specific values within its value objects can be attributed to the model.


## Hats microservice

MODELS:

LocationVO: LocationVO is a value objecrt with respect to the Location model which is located in the wardrobe microservice. The purpose of this model is to relate the Location information to the specific hat identity. The significance of this value object lies in the fact that the hat model directly pertains to the specific location where the hat exists. This relation is made possible through the construction of a ForeignKey in tandem with the poller, which serves to pull the bin data from the wardrobe api and into the hats microservice.

Hat: The hat model serves as an entity in which specific values within its value objects can be attributed to the model.
