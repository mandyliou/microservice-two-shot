import React from 'react';

class HatsForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            fabric: '',
            style_name: '',
            color: '',
            picture_url: '',
            location: '',
            locations: [],
        }
        //when using this method below, make sure the variable matches the data's key
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {//every time you press a key, it's firing
        const value = event.target.value; //value is what you type
        this.setState({ [event.target.name]: value }) //sets state name : value you typed
    }

    async handleSubmit (event) {
        event.preventDefault();
        const data = {...this.state}; //copies all of the properties and values from this.state into a new object
        // data.style_name = data.styleName;
        // data.picture_url = data.pictureUrl;
        // delete data.styleName;
        // delete data.pictureUrl;
        delete data.locations;
        console.log(data);

        // convert href to id
        //data.location = data.location.

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = { //config info about fetch request
            method: "post",
            body: JSON.stringify(data), // data you compiled
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig); //POST request; make a new hat
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat)

            const cleared = {
                fabric: '',
                style_name: '',
                color: '',
                picture_url: '',
                location: '',

            };
            this.setState(cleared);

        }
    }
    async componentDidMount() { //GET
        const url='http://localhost:8100/api/locations/'; //get list of location
        const response = await fetch(url); // default fetch is get

        if (response.ok) {
          const data = await response.json();
          this.setState({locations: data.locations}) // to populate the dropdown

        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.fabric} onChange={this.handleChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.style_name} onChange={this.handleChange} placeholder="Style name" required type="text" name="style_name" id="style_name" className="form-control"/>
                                <label htmlFor="style_name">Style Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.color} onChange={this.handleChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.picture_url} onChange={this.handleChange} placeholder="Picture Url" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                                <label htmlFor="picture_url">Picture Url</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.location} onChange={this.handleChange} required name="location" id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.href}>
                                                {location.closet_name} - {location.section_number}/{location.shelf_number}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );


    }
}
export default HatsForm;
