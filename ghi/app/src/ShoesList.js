import React, { setItems } from "react";


class ShoesList extends React.Component {
    constructor () {
        super()
        this.state = {
            "shoes": []
        }
        this.handleDelete = this.handleDelete.bind(this);
    }

    async componentDidMount() {
        const url = `http://localhost:8080/api/shoes/`;
        let response = await fetch(url);
        if (response.ok) {
            let data = await response.json();
            this.setState({ "shoes":data.shoes })
        }
    }

    async handleDelete(event) {
        const id = event.target.value
        const shoeUrl = `http://localhost:8080/api/shoes/${id}/`;
        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoeList = this.state.shoes.filter(shoe => shoe.id != id)
            this.setState({ "shoes": newShoeList })
        }
    }
    catch (e) {
    }
render() {

    return (
        <>
        <div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Manufacturer</th>
                <th>Model Name</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Bin</th>
            </tr>
            </thead>
            <tbody>
                {this.state.shoes.map(shoe => {
                    return (
                    <tr key={shoe.id}>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.model_name }</td>
                        <td>{ shoe.color }</td>
                        <td><img src={ shoe.picture_url }/></td>
                        <td>{ shoe.bin }</td>
                        <td><button onClick={this.handleDelete} value = {shoe.id} className = "button">Delete</button></td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
        </>
    );
}}





export default ShoesList;
