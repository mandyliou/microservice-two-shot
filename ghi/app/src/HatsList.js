import React from "react";
import "./index.css";


class HatsList extends React.Component {
    constructor() {
        super()
        this.state = {
            "hats": []
        }
        this.handleDelete = this.handleDelete.bind(this);
    }

    async componentDidMount() {
        const url = `http://localhost:8090/api/hats/`;
        let response = await fetch(url);
        if (response.ok) {
            let data = await response.json();
            this.setState({ "hats" : data.hats })
        }
    }

    async handleDelete( event) {
        // event.preventDefault();
        const id = event.target.value
        const hatUrl = `http://localhost:8090/api/hats/${id}/`;
        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHatList = this.state.hats.filter(hat => hat.id != id)
            this.setState({ "hats": newHatList })
        }
    }
    catch (e) {
    }



render() {
    return (
        <>
        <div>
        <table className='table table-striped' >
            <thead>
                <tr>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Picture</th>
                    <th>Location</th>

                </tr>
            </thead>
            <tbody>
                {this.state.hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.fabric }</td>
                            <td><img src={ hat.picture_url } /></td>
                            <td>{ hat.location }</td>
                            <td><button onClick={ this.handleDelete} value = {hat.id}  className = "button">Delete</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </div>
        </>
    );
}}

export default HatsList;
